#!/usr/bin/env python
from sys import argv
import sys
from anki.collection import Collection, ImportAnkiPackageRequest, ImportAnkiPackageOptions
import argparse
from os.path import isfile

parser = argparse.ArgumentParser(
    prog="install_apkgs.py",
    description="Installs specified apks into the system collection")
parser.add_argument('-c', '--collection', help="path to system Anki data")
parser.add_argument('-a', '--apkg', required=True, action='append', help='An apkg to install')
parser.add_argument('-v', '--verbose', required=False, action='store_true', help='print the result of each import')

if __name__ == "__main__":
    args = parser.parse_args()
    if not isfile(args.collection) or not args.collection.endswith('.anki2'):
        raise Exception(f'No .anki2 collection found at {args.collection}, check --collection argument')
    if isfile(f'{args.collection}-wal'):
        raise Exception(f'Cannot import APKGs into {args.collection} because the collection is already open, close Anki and try again')
    for apkg in args.apkg:
        if not apkg.endswith('.apkg'):
            raise Exception(f'{apkg} was given but is not an apkg')
    global_col = Collection(path=args.collection)
    print(f'warning: importing APKGs into {args.collection}, don\'t open collection in Anki or turn off the PC until complete', file=sys.stderr)
    for apkg in args.apkg:
        changes = global_col.import_anki_package(ImportAnkiPackageRequest(
            package_path=apkg,
            options=ImportAnkiPackageOptions(
                merge_notetypes=True,
                update_notes=True,
                update_notetypes=True
            )
        )).log
        if args.verbose:
            print(f'{apkg}: {len(changes.new)} new, {len(changes.updated)} updated, {len(changes.conflicting)} conflicting, {len(changes.duplicate)} duplicates', file=sys.stderr)
    print('APKG import complete', file=sys.stderr)
