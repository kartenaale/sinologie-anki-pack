import { exit } from 'process'
import { announce } from './announce'
import { parseArgs } from 'util'

const { values: { out, title, repo, releaseDir, apkg } } = parseArgs({
  options: {
    out: {
      type: 'string',
      short: 'o'
    },
    title: {
      type: 'string'
    },
    repo: {
      type: 'string',
      short: 'r'
    },
    releaseDir: {
      type: 'string',
      short: 'p'
    },
    apkg: {
      type: 'string',
      multiple: true
    }
  }
})

if (out === undefined ||
    title === undefined ||
    repo === undefined ||
    releaseDir === undefined ||
    apkg === undefined ||
    apkg.length === 0) {
  const given = process.argv.slice(2).join(' ')
  throw new Error(`error: missing arguments, exiting.\nargs:${given}`)
}

void announce({ out, title, repo, releaseDir, apkgFilenames: apkg }).then(
  undefined,
  e => {
    console.error(e)
    exit(1)
  }
)
