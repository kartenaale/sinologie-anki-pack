import { writeFile } from 'fs/promises'
import process from 'process'
import { basename } from 'path'

interface AnnounceOpts {
  /** Output filename */
  out: string
  releaseDir: string
  apkgFilenames: string[]
  /** Pack title, e.g. Tapir Sciences */
  title: string
  /** Full repository name on Gitlab, e.g. kartenaale/sinologie-anki-pack */
  repo: string
}

export async function announce (opts: AnnounceOpts): Promise<void> {
  const version = getReleaseVersion(opts.releaseDir)
  const announcement = [
    `# ${opts.title} ${version} ist released! 🎉`,
    `## Details zum Release:\n${linkToReleaseDetails(opts.repo, version)}`,
    '## Download einzelne APKGs:',
    ...(await linksToApkgs(opts.repo, opts.releaseDir, opts.apkgFilenames))
  ].join('\n\n')
  await writeFile(opts.out, announcement, { encoding: 'utf-8' })
}

function getReleaseVersion (dir: string): string {
  const dashIdx = dir.lastIndexOf('-')
  if (dashIdx < 0) {
    throw new Error(`${dir} does not look like a release dir`)
  }
  return dir.substring(dashIdx + 1)
}

function linkToReleaseDetails (repo: string, version: string): string {
  return (
    `https://gitlab.phaidra.org/${
      repo
    }/-/releases/${
      version
    }`
  )
}

async function linksToApkgs (
  repo: string,
  releaseDir: string,
  apkgFilenames: string[]
): Promise<string[]> {
  const apkgPaths = (
    !(apkgFilenames[0]?.startsWith(`${releaseDir}/`))
  )
    ? apkgFilenames.map(p => `${releaseDir}/${p}`)
    : [...apkgFilenames]
  return apkgPaths.sort().map(path => {
    return `### ${
      basename(path, '.apkg').replace(/-/g, ' ').replace('fuer', 'für')
    }\n${
        rawArtifactDownload(repo, path)
      }`
  })
}

function rawArtifactDownload (repo: string, path: string): string {
  if (path === '') {
    throw new Error('path required')
  }
  return `https://gitlab.phaidra.org/${
    repo
  }/-/jobs/${
    getCiJobId()
  }/artifacts/raw/${encodeURIComponent(path)}`
}

function getCiJobId (): string {
  return process.env.CI_JOB_ID ?? 'job-id-unavailable'
}
