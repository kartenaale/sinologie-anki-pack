import {
  type StrokeType
} from '../../src-common/stroke-encodings'
import {
  type HanziData,
  type TradOverrides
} from '../../src-common/hanzi-data'
import cnchar from 'cnchar'
import cncharOrder from 'cnchar-order'
import cncharRadical from 'cnchar-radical'
import cncharTrad from 'cnchar-trad'
import { getTraditionalInfo } from './traditional'
import {
  queryPatchedHanziWriterData
} from './patched-hanzi-writer'
import { queryFlips } from './query-overrides'
import { queryStrokeTypes } from './stroke-type'
import { queryRadicalInfo } from './radical'

cnchar.use(cncharOrder, cncharRadical, cncharTrad)

export interface QueryOpts {
  readonly char: string
}

export async function query ({ char }: QueryOpts): Promise<HanziData> {
  if (char.length !== 1) {
    throw new Error(`Can only query single chars, got: ${char}`)
  }
  const data = await queryDefault(char)
  const trad = await queryTraditional(char, data)
  if (Object.keys(trad).length > 0) {
    data.trad = trad
  }
  return data
}

async function queryDefault (char: string): Promise<HanziData> {
  const hanziWriterData = await queryPatchedHanziWriterData(char)
  const totalStrokeCount = hanziWriterData.strokes.length
  const simplified = await queryRadicalInfo(char)
  const radical = simplified.radicalInline
  const extra = Math.max(0, totalStrokeCount - simplified.radicalCount)
  const count = `${simplified.radicalCount}+${extra}`
  const strokeTypes = queryStrokeTypes(char, totalStrokeCount)
  if (!checkStrokeTypes(char, strokeTypes, hanziWriterData.strokes)) {
    // if the strokes look odd, leave them out
    strokeTypes.splice(0, strokeTypes.length)
  }
  const data = {
    radical,
    count,
    strokeTypes,
    ...hanziWriterData
  }
  return applyQueryFlipsInPlace(char, data)
}

function checkStrokeTypes (
  char: string,
  strokeShapes: StrokeType[],
  hanziWriterStrokes: string[]
): boolean {
  if (strokeShapes.length === 0) {
    console.error(`stroke table data unavailable for ${char}`)
    return false
  } else if (strokeShapes.length !== hanziWriterStrokes.length) {
    console.error(`${char}: ${
      strokeShapes.length
    } strokes in cnchar, but ${
      hanziWriterStrokes.length
    } in hanzi writer, omitting cnchar stroke shapes`)
    return false
  } else {
    return true
  }
}

function applyQueryFlipsInPlace (char: string, data: HanziData): HanziData {
  for (const positions of queryFlips.get(char) ?? []) {
    flipInPlace(data.strokes, positions)
    flipInPlace(data.medians, positions)
    flipInPlace(data.strokeTypes, positions)
    data.radStrokes = data.radStrokes?.map(i => {
      if (i === positions[0]) {
        return positions[1]
      } else if (i === positions[1]) {
        return positions[0]
      } else {
        return i
      }
    })
  }
  return data
}

function flipInPlace<T> (els: T[], [pos1, pos2]: [number, number]): T[] {
  if (pos1 < 0 || pos1 >= els.length) {
    throw new Error(`out of range: ${pos1}`)
  }
  if (pos2 < 0 || pos2 >= els.length) {
    throw new Error(`out of range: ${pos2}`)
  }
  const temp = els[pos1]
  els[pos1] = els[pos2]
  els[pos2] = temp
  return els
}

async function queryTraditional (
  char: string,
  defaultData: HanziData
): Promise<TradOverrides> {
  const overrides: TradOverrides = {}
  const traditional = await getTraditionalInfo(char, defaultData)
  for (const prop in traditional) {
    if (traditional[prop] !== defaultData[prop]) {
      overrides[prop] = traditional[prop]
    }
  }
  return overrides
}
