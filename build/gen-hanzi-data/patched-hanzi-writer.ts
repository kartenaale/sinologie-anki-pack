import fs from 'fs'
import { applyOverridesInPlace } from './overrides'
import { animDataConcat, animDataSlice, animDataTranslate } from './anim-data'
import { compat } from './blocks/index'

export interface HanziWriterData {
  strokes: string[]
  medians: number[][][]
  radStrokes?: number[]
}
const noHanziWriterData: HanziWriterData = { strokes: [], medians: [] }

export async function queryPatchedHanziWriterData (
  char: string
): Promise<HanziWriterData> {
  // kangxi radicals are often not defined, but CJK ideograms have a good
  // chance, so use that data instead
  const hanziWriterCompatChar = compat(char)
  return applyOverridesInPlace(
    await queryHanziWriterData(hanziWriterCompatChar),
    // use the unchanged char for overrides, not the compatibility one
    hanziWriterOverrides.get(char)
  )
}

export function loadHanziWriterDataWithoutOverridesSync (
  char: string
): Omit<HanziWriterData, 'trad'> {
  const hanziWriterJsonPath = getHanziWriterJsonPath(char)
  if (!fs.existsSync(hanziWriterJsonPath)) {
    throw new Error(`${char} not found`)
  }
  return JSON.parse(
    fs.readFileSync(hanziWriterJsonPath, 'utf-8')
  ) as HanziWriterData
}

async function queryHanziWriterData (
  char: string
): Promise<HanziWriterData> {
  const hanziWriterJsonPath = getHanziWriterJsonPath(char)
  return (
    fs.existsSync(hanziWriterJsonPath)
      ? JSON.parse(
        await fs.promises.readFile(hanziWriterJsonPath, 'utf-8')
      ) as HanziWriterData
      : { ...noHanziWriterData }
  )
}

function getHanziWriterJsonPath (char: string): string {
  return `node_modules/hanzi-writer-data/${char}.json`
}

/**
 * Values to use if HanziWriter has no data, or if it needs to be corrected.
 *
 * Traditional-only hanzi can be corrected here, but if there should be a
 * special version only for traditional mode, see the traditional module.
 */
const hanziWriterOverrides = new Map<string, Partial<HanziWriterData>>(
  Object.entries({
    个: {
    // this was incorrectly set to "radStrokes":[2]
      radStrokes: [0, 1]
    },
    当: {
    // here the top was highlighted instead of the bottom
      radStrokes: [3, 4, 5]
    },
    // here the whole thing is the radical, so no highlighting is more accurate
    干: {
      radStrokes: []
    },
    // radical highlighting not defined in hanziwriter
    来: {
      radStrokes: [3, 4, 5, 6]
    },
    // no rad highlighting defined either
    來: {
      radStrokes: [1, 2]
    },
    半: {
      radStrokes: [3, 4]
    },
    夏: {
      radStrokes: [7, 8, 9]
    },
    前: {
      radStrokes: [7, 8]
    },
    之: {
      radStrokes: [0]
    },
    平: {
      radStrokes: [0, 3, 4]
    },
    整: {
      radStrokes: [7, 8, 9, 10]
    },
    就: {
      radStrokes: [8, 9, 10, 11]
    },
    // defined but wrong (also as component in other chars, maybe later...)
    顛: animDataConcat(
      animDataSlice(
        loadHanziWriterDataWithoutOverridesSync('颠'),
        0,
        10
      ),
      animDataSlice(
        loadHanziWriterDataWithoutOverridesSync('順'),
        3,
        12
      )
    ),
    // not defined at all in hanzi writer, patch it together
    齣: animDataConcat(
      animDataTranslate(
        animDataSlice(
          loadHanziWriterDataWithoutOverridesSync('齬'),
          0, 15
        ),
        -20, 0
      ),
      animDataTranslate(
        animDataSlice(
          loadHanziWriterDataWithoutOverridesSync('夠'),
          6, 11
        ),
        15, 0
      )
    ),
    // not defined
    喫: animDataConcat(
      animDataTranslate(
        animDataSlice(
          loadHanziWriterDataWithoutOverridesSync('哪'),
          0, 3
        ),
        15, -50
      ),
      animDataTranslate(
        animDataSlice(
          loadHanziWriterDataWithoutOverridesSync('楔'),
          4, 13
        ),
        -20, 0
      )
    ),
    // not defined
    靣: dropRadStrokesInPlace(
      animDataConcat(
        animDataConcat(
          animDataSlice(loadHanziWriterDataWithoutOverridesSync('面'), 0, 4),
          animDataTranslate(
            animDataSlice(loadHanziWriterDataWithoutOverridesSync('回'), 2, 5),
            0, -100
          )
        ),
        animDataSlice(loadHanziWriterDataWithoutOverridesSync('面'), 8, 9)
      )
    ),
    // not defined
    襌: animDataConcat(
      animDataTranslate(
        animDataSlice(loadHanziWriterDataWithoutOverridesSync('裸'), 0, 5),
        -30, -50
      ),
      animDataTranslate(
        animDataSlice(loadHanziWriterDataWithoutOverridesSync('彈'), 3, 15),
        30, 0
      )
    ),
    // not defined
    琯: animDataConcat(
      animDataTranslate(
        animDataSlice(
          loadHanziWriterDataWithoutOverridesSync('现'),
          0, 4
        ),
        20, 10
      ),
      animDataTranslate(
        animDataSlice(
          loadHanziWriterDataWithoutOverridesSync('馆'),
          3, 11
        ),
        -20, 0
      )
    ),
    // not defined
    牜: animDataTranslate(
      animDataSlice(
        loadHanziWriterDataWithoutOverridesSync('特'),
        0, 4
      ),
      200, 0
    )
  })
)

function dropRadStrokesInPlace (
  data: Partial<HanziWriterData>
): Partial<HanziWriterData> {
  delete data.radStrokes
  return data
}
