import cnchar from 'cnchar'
import cncharOrder from 'cnchar-order'
import cncharTrad from 'cnchar-trad'
import {
  type StrokeTypeMaybeUnclean,
  StrokeType
} from '../../src-common/stroke-encodings'
import { strokeTypeOverrides } from './stroke-type-overrides'
import { compat } from './blocks/index'
import { normalizeStroke } from './stroke-normalize'

cnchar.use(cncharOrder, cncharTrad)

/**
 * Query stroke shapes for the characters from cnchar and return them in a
 * shortened form.
 *
 * If the stroke count does not match the given expected count, return an empty
 * array instead.
 *
 * Some characters have an override defined, e.g. if cnchar data would be
 * inaccurate.
 *
 * Returns an empty list and prints an error if the expected length is not
 * matched.
 */
export function queryStrokeTypes (
  char: string,
  expectedLen?: number
): StrokeType[] {
  const compatChar = compat(char)
  const override =
    strokeTypeOverrides.get(char) ?? strokeTypeOverrides.get(compatChar)
  if (override !== undefined) {
    return override
  }
  let strokes = queryCncharStrokeTypes(compatChar)
  if (expectedLen !== undefined) {
    strokes = addGrassHengIfMissing(expectedLen, strokes)
  }
  return strokes
}

function queryCncharStrokeTypes (char: string): StrokeType[] {
  const strokeShapes = cnchar.stroke(char, 'order', 'shape')[0]
  if (!Array.isArray(strokeShapes)) {
    return []
  }
  return strokeShapes.map(t =>
    // if cnchar has multiple stroke types, only keep first, and map to a closed
    // set of strokes without duplicate stroke kinds
    normalizeStroke(
      t.split('|')[0] as StrokeTypeMaybeUnclean
    )
  )
}

const traditionalGrassStrokes = [
  StrokeType.SHU,
  StrokeType.HENG,
  StrokeType.SHU,
  StrokeType.HENG
]

/**
 * If the character has one stroke less than expected, and the first strokes
 * are consistent with a simplified grass radical, then return a new array that
 * starts with the four strokes of a traditional grass radical and then
 * continues with the given strokes starting from the fourth stroke.
 *
 * If the length is not one short by one, or if the start does not look like a
 * simplified grass radical, then return the given strokes array unchanged.
 */
function addGrassHengIfMissing (
  expectedLen: number,
  strokes: StrokeType[]
): StrokeType[] {
  const applyPatch =
    // HanziWriter has one more stroke
    (strokes.length + 1) === expectedLen &&
    // and the strokes look like the simplified version
    strokes[0] === StrokeType.HENG &&
    strokes[1] === StrokeType.SHU &&
    strokes[2] === StrokeType.SHU
  return applyPatch
    ? [...traditionalGrassStrokes, ...strokes.slice(3)]
    : strokes
}
