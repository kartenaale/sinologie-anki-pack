export function indexOfSlice<T> (
  inArray: T[],
  findSlice: T[]
): number | undefined {
  for (let i = 0; i < (inArray.length - findSlice.length); ++i) {
    if (isSliceAt(inArray, findSlice, i)) {
      return i
    }
  }
  return undefined
}

function isSliceAt<T> (inArray: T[], slice: T[], position: number): boolean {
  for (let i = 0; i < slice.length; ++i) {
    if (inArray[position + i] !== slice[i]) {
      return false
    }
  }
  return true
}
