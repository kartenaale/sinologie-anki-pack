import { mapString } from './map-string'
import {
  cjkToKangxi,
  cjkToSupplement,
  kangxiToCjk,
  supplementToCjk
} from './mappings'

/**
 * Returns a version of the string that may work better with cnchar and
 * HanziWriter. For example, Kangxi radicals are replaced by CJK unified
 * ideographs.
 *
 * @param input input to build the compat version from, will not be changed
 */
export function compat (input: string): string {
  return mapString(
    input,
    kangxiToCjk,
    supplementToCjk
  )
}

/**
 * Checks if the parameter is a known radical in the Kangxi, CJK ideograph
 * or CJK radicals supplement blocks.
 *
 * May still return false if we don't have a mapping.
 *
 * @param input
 * @returns
 */
export function isKangxiOrCjkRadical (input: string): boolean {
  if (input.length !== 1) {
    throw new Error(`Expected single char input, got: "${input}"`)
  }
  return kangxiToCjk.has(input) ||
    cjkToKangxi.has(input) ||
    supplementToCjk.has(input) ||
    cjkToSupplement.has(input)
}

/**
 * Checks if given char is in the Kangxi radicals block.
 *
 * These do not work well with HanziWriter.
 *
 * @param char to check
 * @see https://www.compart.com/en/unicode/block/U+31C0
 */
export function isKangxiRadical (char: string): boolean {
  if (char.length !== 1) {
    throw new Error(`Single chars only, but got: '${char}'`)
  }
  return /[\u2F00-\u2FDF]/.test(char)
}

/**
 * Checks if in the unified ideographs block.
 *
 * Compared to the Kangxi radical block, this one works better with
 * HanziWriter. Cnchar also tends to have more data for these than for the
 * Kangxi block.
 *
 * @param char to check
 * @see https://www.compart.com/en/unicode/block/U+4E00
 */
export function isCjkUnifiedIdeograph (char: string): boolean {
  if (char.length !== 1) {
    throw new Error(`Single chars only, but got: '${char}'`)
  }
  return /[\u4E00-\u9FFF]/.test(char)
}

export function convertKangxiToCjk (input: string): string {
  return mapString(input, kangxiToCjk)
}

export function convertCjkToKangxi (input: string): string {
  return mapString(input, cjkToKangxi)
}
