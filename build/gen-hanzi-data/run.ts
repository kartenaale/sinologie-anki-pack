import { exit } from 'process'
import { generate } from './generate'
import { parseArgs } from 'util'

const args = parseArgs({
  options: {
    out: {
      type: 'string',
      short: 'o'
    },
    pool: {
      type: 'string',
      multiple: true,
      short: 'p'
    }
  }
})

if (args.values.out === undefined ||
  args.values.pool === undefined ||
  args.values.pool.length === 0) {
  const given = process.argv.slice(2).join(' ')
  throw new Error(`error: missing arguments, exiting.\nargs:${given}`)
}

generate({ pools: args.values.pool, out: args.values.out }).then(
  undefined,
  e => {
    console.error(e)
    exit(1)
  }
)
