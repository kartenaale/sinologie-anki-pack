#!/usr/bin/env python
from anki.collection import Collection, ExportAnkiPackageOptions, ImportAnkiPackageOptions, ImportAnkiPackageRequest
from anki.import_export_pb2 import ImportAnkiPackageUpdateCondition
from anki.models import ChangeNotetypeRequest, NotetypeDict
from anki.notes import Note
from datetime import datetime, timezone
from functools import reduce
from itertools import chain
from libfaketime import fake_time
from pathlib import Path
from pydantic import BaseModel, field_serializer, field_validator
from semver import Version
from tempfile import TemporaryDirectory
from typing import Literal
from yaml import load, CLoader
from os import getenv
import argparse
import csv

APKG_SPEC_FILENAME = '.apkg-spec.yaml'
TEMPLATE_SPEC_FILENAME = '.template-spec.yaml'

parser = argparse.ArgumentParser(
    prog="export_apkgs.py",
    description="Generates apkgs from a spec in a specified directory")
parser.add_argument('-c', '--content', default='content', help="path to YAML or directory tree that contains .apkg-spec.yaml, each spec generates an APKG")
parser.add_argument('-t', '--templates-dir', required=True)
parser.add_argument('-o', '--output-dir', help="directory to put the output apkgs in")
parser.add_argument('-n', '--dry-run', action='store_true', required=False, help="Don't actually build Anki files, but print the files that would be generated")

class NoteType(BaseModel):
    id: datetime
    name: str
    fields: list[str]

class CardType(BaseModel):
    name: str
    template: Path

class ImportApkg(BaseModel):
    note_type: str

class ImportCsv(BaseModel):
    content_version: datetime
    note_type: str
    file_patterns: list[str]|None
    deck_name_pattern: str
    fields_mapping: list[str]
    tags: list[str]

class ImportImages(BaseModel):
    content_version: datetime
    note_type: str
    deck_name_pattern: str
    fields_mapping: dict[str, str]

class TemplateSpec(BaseModel):
    template_version: datetime
    note_type: NoteType
    card_types: list[CardType]
    resource_paths: list[Path]

class ApkgSpec(BaseModel):
    # semver of the content version, appended to the filename after a dash
    content_version: str
    templates: list[str]
    content: list[
        dict[Literal['import_apkg'], ImportApkg]|
        dict[Literal['import_csv'], ImportCsv]|
        dict[Literal['import_images'], ImportImages]
    ]

    @field_serializer('content_version')
    def dump_content_version(self, ver: Version):
        return str(ver)

    @field_validator('content_version')
    def validate_content_version(cls, version):
        Version.parse(version)
        return version


def load_apkg_spec(spec_path: Path) -> ApkgSpec:
    with open(spec_path) as spec_yaml:
        spec_yaml = load(spec_yaml, Loader=CLoader)
        return ApkgSpec(**spec_yaml)

def load_template_spec(templates_dir: Path, template: str) -> TemplateSpec:
    with open(templates_dir / template / TEMPLATE_SPEC_FILENAME) as spec_yaml:
        spec_yaml = load(spec_yaml, Loader=CLoader)
        return TemplateSpec(**spec_yaml)

def format_time(time: datetime):
    """ Formats the given time in a format suitable for faketime """
    return time.astimezone(timezone.utc).strftime('%Y-%m-%d %H:%M:%S')

TEMP_DECK_NAME = 'just_imported'
class ApkgExporter:
    spec: ApkgSpec
    content_dir: Path
    templates_dir: Path
    intermediate_dir: Path
    output_dir: Path
    dry_run: bool
    package_name: str
    """ IDs of note types added by us. """
    added_note_type_ids: list[int]
    added_card_types: set[str]
    resource_pseudo_usages: str

    def __init__(self, spec: ApkgSpec, content_dir: Path, intermediate_dir: Path, args: argparse.Namespace) -> None:
        self.spec = spec
        self.content_dir = content_dir
        self.templates_dir = Path(args.templates_dir)
        self.intermediate_dir = intermediate_dir
        self.output_dir = Path(args.output_dir or '')
        self.dry_run = args.dry_run
        self.package_name = content_dir.name
        self.added_note_type_ids = []
        self.added_card_types = set()
        self.resource_pseudo_usages = ''


    def export(self) -> Path:
        apkg_filename = self.output_dir / f'{self.package_name}-{self.spec.content_version}.apkg'
        if self.dry_run:
            print(str(apkg_filename))
            return
        
        collection = self.empty_collection()
        name_to_ntid = self.add_note_types(collection)
        self.add_content(collection, name_to_ntid)
        collection.export_anki_package(
            # anki fails if this is just an apkg without a dir, but with ./ it's fine
            out_path=f'./{apkg_filename}' if apkg_filename.parent == Path('.') else str(apkg_filename),
            options=ExportAnkiPackageOptions(
                with_deck_configs=False,
                with_media=True,
                with_scheduling=False,
                legacy=True,
            ),
            limit=None)
        return apkg_filename

    def empty_collection(self) -> Collection:
        collection_filename = self.intermediate_dir / f'{self.package_name}.anki2'
        col = Collection(collection_filename)
        # delete default content as far as possible (ID 1 cannot be deleted)
        self.clear_other_note_types(col)
        return col

    def clear_other_note_types(self, col: Collection):
        """
        Deletes all note types in the collection except the ones added during
        export. This will clean up default content and unwanted note types that
        are imported from APKGs that were just used for their content.
        """
        for model in col.models.all_names_and_ids():
            if model.id not in self.added_note_type_ids:
                col.models.remove(model.id)

    def add_note_types(self, col: Collection) -> dict[str, int]:
        name_to_ntid = {}
        template_specs = [ (template, load_template_spec(self.templates_dir, template)) for template in self.spec.templates ]
        for template_name, template_spec in template_specs:
            note_type = self.add_note_type(col, template_spec.note_type)
            if note_type["name"] in name_to_ntid:
                    raise Exception(f'duplicate note type name {note_type["name"]}')
            name_to_ntid[note_type["name"]] = note_type["id"]
            note_type["tmpls"] = [] # clear out dummy card

            # start by adding resources
            with fake_time(format_time(template_spec.template_version)):
                for resources_path in template_spec.resource_paths:
                    # for resources generated using build,
                    # {{BUILD_PREFIX}} may be used
                    resources_path = Path(
                        str(resources_path)
                            .replace('{{BUILD_PREFIX}}', getenv('BUILD_PREFIX', '')
                    ))
                    for resource_path in resources_path.glob('*'):
                        actual_path = col.media.add_file(resource_path)
                        if actual_path != str(resource_path.name):
                            raise Exception(f'resource was renamed by Anki from {resource_path} to {actual_path}, expected {resource_path.name} instead')
                        # convince anki to include media in export
                        self.resource_pseudo_usages += f'<img src="{actual_path}">'

            # then add the card types with the html
            final_mod_date = template_spec.note_type.id
            for card_type_spec in template_spec.card_types:
                if card_type_spec.name in self.added_card_types:
                    raise Exception(f'duplicate card type name {card_type_spec.name}')
                self.added_card_types.add(card_type_spec.name)
                card_type = self.create_card_type(template_name, card_type_spec)
                note_type["tmpls"].append(card_type)
                with fake_time(format_time(template_spec.template_version)):
                    col.models.update_dict(note_type)
                final_mod_date = max(template_spec.template_version, final_mod_date)
            with fake_time(format_time(final_mod_date)):
                note_type['mod'] = round(final_mod_date.timestamp() * 1000)
                col.models.update_dict(note_type)
        return name_to_ntid

    def add_note_type(self, col: Collection, spec: NoteType) -> NotetypeDict:
        name = spec.name
        fields = list(spec.fields)
        id_as_utc_microseconds = round(spec.id.timestamp() * 1000)
        id_as_local_naive_date = format_time(spec.id)
        resource_field_id = None
        if not "resources" in fields:
            fields.append("resources")
            resource_field_id = 7681557096
        else:
            resource_field_id = -id_as_utc_microseconds + fields.index("resources")
        # the anki collection ID is set from the time of adding, so we fake that time
        with fake_time(id_as_local_naive_date):
            note_type_id = col.models.add_dict({
                'id': 0,
                'name': self.format_note_type_name(name),
                'type': 0,
                'mod': 0,
                'usn': 0,
                'sortf': 0,
                'did': None,
                # placeholder to be non-empty, the real templates are added later
                # with custom modification date
                'tmpls': [
                    {
                        'name': 'DUMMY',
                        'ord': 0,
                        'qfmt': '{{' + fields[0] + '}}',
                        'afmt': '{{' + fields[max(1, len(fields) - 1)] + '}}',
                        'bqfmt': '',
                        'bafmt': '',
                        # this could maybe be the deck override option
                        'did': None,
                        'bfont': '',
                        'bsize': 0,
                        # id?
                    }
                ],
                'flds': [ self.create_field(idx, -id_as_utc_microseconds + idx if field != "resources" else resource_field_id, field) for idx, field in enumerate(fields) ],
                'css': '',
                'latexPre':'\\documentclass[12pt]{article}\n\\special{papersize=3in,5in}\n\\usepackage[utf8]{inputenc}\n\\usepackage{amssymb,amsmath}\n\\pagestyle{empty}\n\\setlength{\\parindent}{0in}\n\\begin{document}\n',
                'latexPost':'\\end{document}',
                'latexSvg': False,
                'req': [
                    [
                        0,
                        'any',
                        [ 0 ]
                    ]
                ],
                'originalStockKind': 1
            }).id
        if note_type_id != id_as_utc_microseconds:
            expected = datetime.fromtimestamp(id_as_utc_microseconds / 1000)
            actual = datetime.fromtimestamp(note_type_id / 1000)
            raise Exception(f'Expected note ID {id_as_utc_microseconds} ({expected}) but got {note_type_id} ({actual})')
        self.added_note_type_ids.append(note_type_id)
        return col.models.get(note_type_id)

    def create_card_type(self, template_name: str, card_type_spec: CardType) -> dict:
        name = card_type_spec.name
        template_dir = self.templates_dir / template_name / card_type_spec.template
        front_html_path = template_dir / 'front.html'
        back_html_path = template_dir / 'back.html'
        with open(front_html_path) as front_html_file:
            with open(back_html_path) as back_html_file:
                front_html = front_html_file.read()
                back_html = back_html_file.read()
                return {
                    'name': name,
                    'ord': 0,
                    'qfmt': front_html,
                    'afmt': back_html,
                    'bqfmt': '',
                    'bafmt': '',
                    # this could maybe be the deck override option
                    'did': None,
                    'bfont': '',
                    'bsize': 0,
                    # id?
                }

    def create_field(self, ord: int, id: int, field: str) -> dict:
        return {
            'name': field,
            'ord': ord,
            'sticky': False,
            'rtl': False,
            # not sure what the font stuff does, should be styled in CSS
            'font': 'Arial',
            'size': 20,
            'description': '',
            'plainText': False,
            'collapsed': False,
            'excludeFromSearch': False,
            'id': id,
            'tag': None,
            'preventDeletion': False
        }

    def add_content(self, col: Collection, name_to_ntid: dict[str, int]):
        tmp_deck = self.add_tmp_deck(col)
        for content in self.spec.content:
            if 'import_csv' in content:
                import_csv : ImportCsv = content['import_csv']
                deck_name = import_csv.deck_name_pattern
                fields_mapping = import_csv.fields_mapping
                ntid = name_to_ntid[import_csv.note_type]
                with fake_time(format_time(import_csv.content_version)):
                    for pattern in import_csv.file_patterns or ['**/*.csv']:
                        for csv_path in self.content_dir.glob(pattern):
                            self.add_csv(
                                col,
                                ntid,
                                tmp_deck,
                                import_csv.tags,
                                csv_path,
                                fields_mapping)
                            self.move_to_target_decks(col, tmp_deck, deck_name, csv_path)
            elif 'import_apkg' in content:
                import_apkg : ImportApkg = content['import_apkg']
                ntid = name_to_ntid[import_apkg.note_type]
                for apkg_path in self.content_dir.glob('**/*.apkg'):
                    self.add_apkg(col, apkg_path, ntid)
            elif 'import_images' in content:
                IMAGES = reduce(
                    chain,
                    [
                        chain(
                            self.content_dir.glob(f'**/*.{ext}'),
                            self.content_dir.glob(f'**/*.{ext.upper()}')
                        ) for ext in [ 'jpg', 'jpeg', 'png', 'gif' ]
                    ],
                    [])
                import_images : ImportImages = content['import_images']
                ntid = name_to_ntid[import_images.note_type]
                with fake_time(format_time(import_images.content_version)):
                    for image_path in IMAGES:
                        self.add_image(col, import_images, ntid, tmp_deck, image_path)
                        self.move_to_target_decks(col, tmp_deck, import_images.deck_name_pattern, image_path)
            else:
                raise Exception(f'I don\'t know how to import this: {content}')
        col.decks.remove([tmp_deck])

    def add_tmp_deck(self, col: Collection) -> int:
        return col.decks.add_normal_deck_with_name(TEMP_DECK_NAME).id

    def get_or_create_deck_id(self, col: Collection, deck_name: str):
        deck = col.decks.by_name(deck_name)
        if deck is None:
            deck_id = col.decks.add_normal_deck_with_name(deck_name).id
            deck = col.decks.get(deck_id)
            if deck is None:
                raise Exception(f'Failed to crate deck {deck_name}')
        return deck['id']

    def add_csv(
            self,
            col: Collection,
            note_type_id: int,
            tmp_deck: int,
            tags: list[str],
            csv_path: Path,
            fields_mapping: list[str]):
        with open(csv_path) as csv_file:
            reader = csv.reader(csv_file, delimiter=';')
            for row in reader:
                note = Note(col, note_type_id)
                note = self.new_note(col, row[fields_mapping.index('guid')], note_type_id)
                for idx, field in enumerate(fields_mapping):
                    if idx >= len(row):
                        raise Exception(f'CSV row is missing {field} at index {idx}:\n{row}')
                    if field != 'guid':
                        note[field] = row[idx]
                for tag in tags:
                    note.add_tag(tag)
                col.add_note(note, tmp_deck)

    def field_columns(self, col: Collection, note_type_id: int, mapping: list[str]) -> list[int]:
        """
        Maps the ordinal of a field against the 1-based index in the CSV.
        Fields that are not found in the CSV are set to 0.
        """
        model_fields = col.models.get(note_type_id)['flds']
        fields : list[int] = [-1] * len(model_fields)
        for model_field in model_fields:
            model_name = model_field['name']
            model_ord = model_field['ord']
            csv_index = mapping.index(model_name) + 1 if model_name in mapping else 0
            fields[model_ord] = csv_index
        return fields

    def add_apkg(self, col: Collection, apkg_path: Path, built_note_type_id: int):
        report = col.import_anki_package(ImportAnkiPackageRequest(
            package_path=str(apkg_path),
            options=ImportAnkiPackageOptions(
                merge_notetypes=True,
                # fail later from log when this happens
                update_notes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_ALWAYS,
                # never import note types, we make them ourselves during build
                # anki will map them somehow (from the name?)
                update_notetypes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_NEVER,
                with_scheduling=False,
                with_deck_configs=False,
            )
        ))
        if report.log.updated:
            raise Exception('unexpected note update')
        if report.log.conflicting:
            raise Exception('unexpected note conflict')
        if report.log.missing_notetype:
            raise Exception('unexpected note missing notetype')
        if report.log.missing_deck:
            raise Exception('unexpected missing deck')
        if report.log.empty_first_field:
            raise Exception('unexpected empty first field')
        if report.log.new:
            new_nids = [ new.id.nid for new in report.log.new ]
            first_note = col.get_note(new_nids[0])
            import_note_type_id = first_note.mid
            info = col.models.change_notetype_info(
                old_notetype_id=import_note_type_id,
                new_notetype_id=built_note_type_id)
            req = ChangeNotetypeRequest(
                note_ids=new_nids,
                new_fields=info.input.new_fields,
                new_templates=info.input.new_templates,
                old_notetype_id=info.input.old_notetype_id,
                new_notetype_id=info.input.new_notetype_id,
                current_schema=info.input.current_schema,
                old_notetype_name=info.input.old_notetype_name,
                is_cloze=info.input.is_cloze
            )
            changes = col.models.change_notetype_of_notes(req)
        # delete models created during import
        self.clear_other_note_types(col)

    def move_to_target_decks(self,
            col: Collection,
            tmp_deck_id: int,
            deck_name_pattern: str,
            csv_path: Path):
        for card_id in col.find_cards('deck:"' + TEMP_DECK_NAME + '"'):
            card = col.get_card(card_id)
            card_type_name = card.template()["name"]
            desired_deck_name = self.format_deck_name(deck_name_pattern, csv_path, card_type_name)
            card.did = self.get_or_create_deck_id(col, desired_deck_name)
            col.update_card(card)

    def new_note(self, col: Collection, guid: str, note_type_id: int):
        note = Note(col, note_type_id, None)
        note.guid = guid
        if self.resource_pseudo_usages:
            # would be nicer to do actual dependency resolution here, but we just dump into an otherwise unused field on the first note created
            note["resources"] = self.resource_pseudo_usages
            self.resource_pseudo_usages = ""
        return note

    def add_image(self,
            col: Collection,
            spec: ImportImages,
            note_type_id: int,
            tmp_deck_id: int,
            image_path: Path):
        anki_image_path = col.media.add_file(str(image_path))
        note = self.new_note(col, str(image_path), note_type_id)
        for field, pattern in spec.fields_mapping.items():
            note[field] = self.format_image_card_field(pattern, image_path, anki_image_path)
        col.add_note(note, tmp_deck_id)

    def format_note_type_name(self, pattern):
        replacements = {
            'content_version': self.spec.content_version
        }
        for key, value in replacements.items():
            pattern = pattern.replace('{{' + key + '}}', value)
        return pattern

    def format_image_card_field(self, pattern: str, orig_image_path: Path, anki_image_path: str):
        replacements = {
            'from_basename': orig_image_path.stem.replace('__', '::').replace('␣', ' '),
            'image': f'<img src="{anki_image_path}" alt="{orig_image_path.stem}">',
            'content_version': self.spec.content_version
        }
        for key, value in replacements.items():
            pattern = pattern.replace('{{' + key + '}}', value)
        return pattern

    def format_deck_name(self, pattern: str, csv_path: Path|None, card_type_name: str|None) -> str:
        replacements = {
            'content_folder': self.package_name,
            'content_version': self.spec.content_version
        }
        if csv_path:
            replacements['from_basename'] = csv_path.stem.replace('__', '::').replace('␣', ' ')
        if card_type_name:
            replacements['card_type'] = card_type_name
        for key, value in replacements.items():
            pattern = pattern.replace('{{' + key + '}}', value)
        return pattern

def export_package_from_spec(spec_path: Path, args: argparse.Namespace) -> Path:
    with TemporaryDirectory() as intermediate_dir:
        package_directory = spec_path.parent
        spec = load_apkg_spec(spec_path)
        exporter = ApkgExporter(
            spec=spec,
            content_dir=package_directory,
            intermediate_dir=Path(intermediate_dir),
            args=args
        )
        return exporter.export()

if __name__ == "__main__":
    args = parser.parse_args()
    content = Path(args.content)
    if content.suffix == '.yaml' or content.suffix == '.yml':
        # directly specified a YAML as argument
        export_package_from_spec(content, args)
    else:
        # got a directory or directory tree to find specs in
        for apkg_spec_path in Path(args.content).glob(f'**/{APKG_SPEC_FILENAME}'):
            export_package_from_spec(apkg_spec_path, args)
