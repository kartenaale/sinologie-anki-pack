# Sinologie Anki Pack User Guide (Deutsch)

## Was ist dieses Sinologie Anki Pack?

Dieser Abschnitt gibt einen Überblick über das Pack. Du kannst auch direkt mit der [Schnellstartanleitung](#schnellstartanleitung) beginnen.

Das _Sinologie Anki Pack_ enthält verschiedene Decks für das Lernprogramm _Anki_, die dir im Sinologiestudium an der Uni Wien helfen können. Du kannst damit _Vokabeln_ und _Fakten_ lernen.

Mitgeliefert werden:

*   Vokabeln, Radikale, Zeichenkomponenten, mit Langzeichen und Kurzzeichen:
    * _Chinesisch lernen für die Sinologie (Band 1)_
    * _Chinesisch lernen für die Sinologie (Band 2)_
*   Fakten:
    * Introduction to Chinese Cultural History
    * Chinesische Politische Geschichte

Es gibt folgende Arten von Karten:

* Frage/Antwort (Fakten),
* Gesprochenes Chinesisch verstehen,
* Hànzì und Pīnyīn lesen,
* Hànzì schreiben,
* deutsche Begriffe auf Chinesisch übersetzen,
* Radikale erkennen.

Einige Features:

* Strichfolge-Animationen,
* Berühre ein Zeichen für Strichfolge-Details inkl. Nummerierung,
* Lege eigene Karten an und bekomme Radikal- und Hörverständniskarten automatisch zu den klassischen Vokabelkarten.

Kartenaale treffen sicher unter anderem auf [Whatsapp](https://chat.whatsapp.com/JFKpfmq29yM2xKcSu7JQib).

Schnellstartanleitung
---------------------

Um einfach nur zu üben, folge diesem Abschnitt.

### Schritt 1: apkg herunterladen

Lade die neueste _apkg_\-Datei vom GitLab-Repository unter [Releases](https://gitlab.phaidra.org/kartenaale/sinologie-anki-pack/-/releases) auf einen PC herunter, z.B. `sinologie-anki-pack-1.1.9.zip`. Im Release findest du die Decks als APKG-Dateien, z.B. _Chinesisch lernen für die Sinologie-2.0.0.apkg_.

### Schritt 2: Anki Desktop intallieren

Wenn du Anki noch nicht auf deinem PC installiert hast, lade es von der offiziellen [Anki](https://apps.ankiweb.net/#download)\-Seite herunter und installiere.

### Schritt 3: apkg importieren

Importiere das _apkg_\-Deck in Anki Desktop, z.B. unter _File | Import…_:

![_File | Import…_ dialog in Anki Desktop](screenshots/screenshot-anki-import.png)

![_sinologie-anki-pack-1.1.9.zip_ in the file open dialog of Anki Desktop](screenshots/screenshot-anki-import-dialog.png)

Damit ist alles erledigt um am PC zu lernen. Probiere es aus!

### Schritt 4: Anki am Smartphone (optional)

Schön an Anki ist, dass man auch am Smartphone oder anderen Geräten lernen kann. Du kannst diesen Schritt überspringen falls du das (noch) nicht brauchst, oder du kannst es später erledigen.

Stelle zuerst sicher, dass deine Kartensammlung mit Anki Web synchron ist. Betätige dazu den _Sync_\-Knopf open rechts. Du musst dich registrieren, falls noch nicht geschehen.

Wenn die Synchronisierung abgeschlossen ist, kannst du über einen Web-Browser auf deinem Smartphone unter [https://ankiweb.net/](https://ankiweb.net/) lernen. Wenn du eine App bevorzugst, probiere [AnkiDroid](https://play.google.com/store/apps/details?id=com.ichi2.anki&hl=de_AT&gl=US&pli=1) (for Android-Handys, gratis) oder [Anki Mobile](https://apps.apple.com/de/app/ankimobile-flashcards/id373493387) (mobile Apple-Geräte, z.B. iPhone or iPad, ~30€).

### Schritt 5: Vorlese-Funktion in AnkiDroid konfigurieren (optional)

Bei Benutzung von AnkiDroid wird empfohlen Text-to-speech unter _Settings | Advanced | Text to speech_ zu aktivieren. Wie bei der Desktop-App werden dann bei den Zuhör-Übungen chinesische Begriffe laut abgespielt. Für andere Karten-Typen erfolgt die Sprachausgabe auf der Rückseite der Karteikarte. Um erneut abzuspielen, gibt es in _AnkiDroid_ einen Play-Button in der Menüleiste oben. Der Button ist möglicherweise ausgeblendet. Er lässt sich unter _Settings | Reviewing | App bar buttons | Replay audio_ einblenden, indem man die Einstellung auf _Always show_ setzt.

Auf anderen Plattformen wie _AnkiWeb_, _Anki Mobile_ oder auf der PC-Version von Anki ist keine Konfiguration erforderlich. Hier kann der Play-Button auf der Karte verwendet werden.

### Step 5: Genießen!

Das war's, viel Spaß beim lernen!

Fortgeschrittene Anwendung
--------------------------

### Wie kann ich mir die Strichfolge für ein chinesisches Zeichen ansehen?

Wenn du ein animiertes chinesisches Zeichen siehst, kannst du darauf klicken bzw. es berühren. Es öffnet sich eine Tabelle mit den einzelnen Strichen, z.B.: ![Screenshot of the template in AnkiDroid](screenshots/screenshot-practice-writing.png)

### Auf eine neuere Version updaten

Wenn eine neuere Version als apkg verfügbar ist und du updaten möchtest, folge noch einmal den Schritten oben. Dein bisheriger Fortschritt bleibt auf jeden
Fall erhalten, aber bei größeren Updates werden die Karten als ein neues Deck
mit einer neuen Versionsnummer abgelegt. Du kannst dann entweder die alte
Version löschen, oder nur manche der neuen Karten behalten wenn du deinen
Fortschritt lieber behalten möchtest.

### Wenn ich ein Smartphone verwende, brauche ich dann trotzdem die PC-Version?

Nicht unbedingt, du kannst das APKG auch auf dein Smartphone laden und dort in AnkiDroid hinzufügen. Verwende dazu "Import" oben rechts:

![APKG in AnkiDroid 1](screenshots/phone-import-1.jpg)

![APKG in AnkiDroid 2](screenshots/phone-import-2.jpg)

![APKG in AnkiDroid 3](screenshots/phone-import-3.jpg)

### Fortgeschrittene Anwendung und Anpassung

Außer deine eigenen Inhalte hinzuzufügen, kannst du auch viele andere Dinge direkt in Anki für dich anpassen. Zum Beispiel können kleinere optische Veränderungen wie eine Änderung der Schriftgröße mit CSS gemacht werden. Für tiefergehende Anpassungen oder neue Features, siehe auch das README im [Repository](https://gitlab.phaidra.org/kartenaale/sinologie-anki-pack).

## Zum Kartenpack beitragen

Du kannst die Decks auch leicht mit deinen eigenen Vokabeln oder Fakten erweitern oder eigene Decks anlegen. Wenn du mit dem Ergebnis zufrieden bist, überlege dir ob du es mit anderen Kartenaalen teilen möchtest. Die Früchte deiner Arbeit könnten letzten Endes hier landen, wenn du das möchtest.

Du kannst gern ein GitLab-Issue anlegen wenn du Schwierigkeiten bei der Verwendung hast. Wenn du selbst Features hinzufügen möchtest, lege ein Issue an oder mache direkt einen Pull-Request auf

Für Verbesserungsvorschläge auch gerne Issues anlegen.

Wenn du selbst kein sehr technischer Benutzer bist aber trotzdem neue Wörter hinzufügen willst, kannst du auch gerne ein Issue mit deinen Wörtern anlegen und ich helfe dabei.