.DELETE_ON_ERROR:
include \
make/defs.mk \
make/src-common.mk \
make/pipenv.mk \
make/node.mk \
make/hanzi-data.mk \
make/changelogs.mk \
make/html.mk \
make/apkgs.mk \
make/docs.mk \
make/announce.mk \
make/eslint.mk \
make/release.mk \
make/install.mk \
make/global.mk