import { StrokeType } from '../../src-common/stroke-encodings'
import {
  GetHanziDataKind,
  getHanziData
} from '../../src/components/hanzi-data'

describe('stroke counts', () => {
  describe('default kind', () => {
    const kind = GetHanziDataKind.DEFAULT
    const testCases = [
      { character: '错', expectedCount: '5+8' },
      { character: '你', expectedCount: '2+5' },
      { character: '笔', expectedCount: '6+4' }
    ]
    for (const testCase of testCases) {
      test(`stroke count of ${testCase.character}`, async () => {
        const data = await getHanziData({
          char: testCase.character,
          kind
        })
        const actualCount = data?.count
        expect(actualCount).toBe(testCase.expectedCount)
      })
    }
  })
  describe('traditional kind', () => {
    const kind = GetHanziDataKind.TRADITIONAL
    const testCases = [
      { character: '紫', expectedCount: '6+6' },
      { character: '幫', expectedCount: '3+14' },
      { character: '處', expectedCount: '6+5' },
      { character: '夏', expectedCount: '3+7' },
      { character: '鄰', expectedCount: '7+12' },
      { character: '圜', expectedCount: '3+13' },
      { character: '璮', expectedCount: '5+13' },
      { character: '刷', expectedCount: '2+6' },
      { character: '戀', expectedCount: '4+19' },
      { character: '矮', expectedCount: '5+8' },
      { character: '驚', expectedCount: '10+13' },
      { character: '雞', expectedCount: '8+10' },
      { character: '顱', expectedCount: '9+16' },
      { character: '襪', expectedCount: '6+15' },
      { character: '勵', expectedCount: '2+15' },
      { character: '藝', expectedCount: '6+15' },
      { character: '歡', expectedCount: '4+18' },
      { character: '戰', expectedCount: '4+12' },
      { character: '翻', expectedCount: '6+12' },
      { character: '敵', expectedCount: '4+11' },
      { character: '彎', expectedCount: '3+19' },
      { character: '籍', expectedCount: '6+14' },
      { character: '餓', expectedCount: '9+7' },
      { character: '隨', expectedCount: '8+13' },
      { character: '翼', expectedCount: '6+11' },
      { character: '響', expectedCount: '9+11' },
      { character: '矊', expectedCount: '5+14' },
      { character: '聲', expectedCount: '6+11' },
      { character: '磬', expectedCount: '5+11' },
      { character: '獸', expectedCount: '4+15' },
      { character: '義', expectedCount: '6+7' },
      { character: '登', expectedCount: '5+7' },
      { character: '灣', expectedCount: '4+22' },
      { character: '驢', expectedCount: '10+16' },
      { character: '壓', expectedCount: '3+14' },
      { character: '避', expectedCount: '7+13' },
      { character: '甩', expectedCount: '5+0' },
      { character: '懈', expectedCount: '4+13' },
      { character: '靈', expectedCount: '8+16' },
      { character: '攤', expectedCount: '4+19' },
      { character: '氧', expectedCount: '4+6' },
      { character: '參', expectedCount: '2+9' },
      { character: '熱', expectedCount: '4+11' },
      { character: '餐', expectedCount: '9+7' },
      { character: '虝', expectedCount: '6+6' },
      { character: '聽', expectedCount: '6+16' }
    ]
    for (const testCase of testCases) {
      test(`stroke count of ${testCase.character}`, async () => {
        const data = await getHanziData({
          char: testCase.character,
          kind
        })
        const actualCount = data?.count
        expect(actualCount).toBe(testCase.expectedCount)
      })
    }
  })
})

describe('stroke types', () => {
  const testCases = [
    {
      hanzi: '艸',
      expected: [
        StrokeType.SHUZHE,
        StrokeType.SHU,
        StrokeType.PIE,
        StrokeType.SHUZHE,
        StrokeType.SHU,
        StrokeType.SHU
      ]
    }
  ]
  for (const { hanzi, expected } of testCases) {
    test(hanzi, async () => {
      const { strokeTypes } = await getHanziData({ char: hanzi })
      expect(strokeTypes).toEqual(expected)
    })
  }
})

describe('radicals', () => {
  describe('default kind', () => {
    const kind = GetHanziDataKind.DEFAULT
    const testCases = [
      ['王', '王'],
      ['了', '乙'],
      ['草', '艹'],
      ['笔', '竹']
    ]
    for (const [hanzi, expectedRadical] of testCases) {
      test(hanzi, async () => {
        const data = await getHanziData({ char: hanzi, kind })
        const radical = data?.radical
        expect(radical).toBe(expectedRadical)
      })
    }
  })
  describe('omitted kind', () => {
    test('了', async () => {
      // no kind also means default
      const data = await getHanziData({ char: '了' })
      const radical = data?.radical
      expect(radical).toBe('乙')
    })
  })
  describe('traditional kind', () => {
    const kind = GetHanziDataKind.TRADITIONAL
    const testCases = [
      ['王', '玉'],
      ['了', '亅'],
      ['蘭', '艸'],
      ['聽', '耳'],
      ['笔', '竹'],
      ['齣', '齒']
    ]
    for (const [hanzi, expectedRadical] of testCases) {
      test(hanzi, async () => {
        const data = await getHanziData({ char: hanzi, kind })
        const radical = data?.radical
        expect(radical).toBe(expectedRadical)
      })
    }
  })
})

describe('SVG data', () => {
  const mustHave = [
    '凵',
    '季',
    '木',
    '纔',
    '裏',
    '這',
    '阝',
    '餵',
    '鼕',
    '齒'
  ]
  for (const hanzi of mustHave) {
    test(hanzi, async () => {
      const data = await getHanziData({ char: hanzi })
      expect(data?.strokes?.length).toBeGreaterThan(0)
    })
  }
})

describe('SVG data for grass radicals default vs. traditional', () => {
  const grassChars = [
    '若',
    '草',
    '花',
    '苦',
    '莫',
    '苗'
  ]
  for (const grassChar of grassChars) {
    test(grassChar, async () => {
      const defaultData = await getHanziData({ char: grassChar })
      const tradData = await getHanziData({
        char: grassChar,
        kind: GetHanziDataKind.TRADITIONAL
      })
      expect(tradData.strokes.length).toBe(defaultData.strokes.length + 1)
      expect(tradData.strokeTypes.slice(0, 4)).toEqual([
        StrokeType.SHU,
        StrokeType.HENG,
        StrokeType.SHU,
        StrokeType.HENG
      ])
    })
  }
})
