import { isMaybeHanzi } from '../../src/components/is-hanzi'

describe('accepts Hanzi', () => {
  const samples = [
    '⽱',
    '⻊',
    '貘',
    '貘⻊⽱'
  ]
  for (const sample of samples) {
    test(sample, () => {
      expect(isMaybeHanzi(sample)).toBe(true)
    })
  }
})

describe('rejects Latin characters, umlauts and punctuation', () => {
  const samples = [
    '', // empty string is also not hanzi
    'asdfÄÜ?…',
    '…',
    '-–—'
  ]
  for (const sample of samples) {
    test(sample, () => {
      expect(isMaybeHanzi(sample)).toBe(false)
    })
  }
})
