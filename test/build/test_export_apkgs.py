from anki.collection import Collection, ImportAnkiPackageRequest, ImportAnkiPackageOptions
from anki.import_export_pb2 import ImportAnkiPackageUpdateCondition
from build.export_apkgs import export_package_from_spec
from dataclasses import dataclass
from pathlib import Path
from tempfile import TemporaryDirectory
import unittest

@dataclass
class MockArgs:
    content: str
    templates_dir: str
    output_dir: str
    dry_run: bool

CONTENT_PATH_ANKI = 'test/build/fixtures/anki'
CONTENT_PATH_CSV = 'test/build/fixtures/csv'
CONTENT_PATH_CSV_UPDATED_CONTENT = 'test/build/fixtures/csv_updated_content'
TEMPLATES_PATH = 'test/build/fixtures/templates'
TEMPLATES_PATH_UPDATED = 'test/build/fixtures/templates_updated'

class TestExportApkgs(unittest.TestCase):
    def test_generate_once_and_reimport(self):
        """
        Basic sanity check: if the exported package is imported twice,
        everything is a duplicate and nothing should change.
        """
        with TemporaryDirectory() as temp_collection_dir:
            col = Collection(str(Path(temp_collection_dir) / "test.anki2"))
            with TemporaryDirectory() as first_export_dir:
                args_first = MockArgs(
                    content=CONTENT_PATH_CSV,
                    templates_dir=TEMPLATES_PATH,
                    output_dir=first_export_dir,
                    dry_run=False)
                package = export_package_from_spec(
                    Path(CONTENT_PATH_CSV) / '.apkg-spec.yaml',
                    args_first)

                # import the first time, everything is new
                result1 = col.import_anki_package(ImportAnkiPackageRequest(
                    package_path=str(package),
                    options=ImportAnkiPackageOptions(
                        merge_notetypes=True,
                        update_notes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                        update_notetypes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                        with_scheduling=False,
                        with_deck_configs=False,
                    )
                ))
                self.assertEqual(len(result1.log.new), 1)
                self.assertEqual(len(result1.log.duplicate), 0)

                # now import again, nothing should change
                result2 = col.import_anki_package(ImportAnkiPackageRequest(
                    package_path=str(package),
                    options=ImportAnkiPackageOptions(
                        merge_notetypes=True,
                        update_notes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                        update_notetypes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                        with_scheduling=False,
                        with_deck_configs=False,
                    )
                ))
                self.assertEqual(len(result2.log.duplicate) == 1 and len(result2.log.new), 0,
                    f'Expected the note to be recognized as duplicate.\nLog1:\n{result1.log}\nLog2:\n{result2.log}')

    def test_generate_twice_and_reimport(self):
        """
        This checks that if we generate the package twice and import it twice,
        then the second import will not change anything because the content is
        supposed to be the same.

        If we introduce errors that lead to different note and card IDs being
        generated, then this test will fail.
        """
        with TemporaryDirectory() as temp_collection_dir:
            col = Collection(str(Path(temp_collection_dir) / "test.anki2"))
            with TemporaryDirectory() as first_export_dir:
                args_first = MockArgs(
                    content=CONTENT_PATH_CSV,
                    templates_dir=TEMPLATES_PATH,
                    output_dir=first_export_dir,
                    dry_run=False)
                with TemporaryDirectory() as second_export_dir:
                    args_second = MockArgs(
                        content=CONTENT_PATH_CSV,
                        templates_dir=TEMPLATES_PATH,
                        output_dir=second_export_dir,
                        dry_run=False)
                    spec_path = Path(CONTENT_PATH_CSV) / '.apkg-spec.yaml'
                    first_package = export_package_from_spec(spec_path, args_first)
                    second_package = export_package_from_spec(spec_path, args_second)
                    
                    # debug: uncomment to view for testing
                    # shutil.copy(first_package, './test-export-first.apkg.zip')
                    # shutil.copy(second_package, './test-export-second.apkg.zip')

                    # import the first time, everything is new
                    result1 = col.import_anki_package(ImportAnkiPackageRequest(
                        package_path=str(first_package),
                        options=ImportAnkiPackageOptions(
                            merge_notetypes=True,
                            update_notes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            update_notetypes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            with_scheduling=False,
                            with_deck_configs=False,
                        )
                    ))
                    self.assertEqual(len(result1.log.new), 1)
                    self.assertEqual(len(result1.log.duplicate), 0)

                    # now import again, nothing should change
                    result2 = col.import_anki_package(ImportAnkiPackageRequest(
                        package_path=str(second_package),
                        options=ImportAnkiPackageOptions(
                            merge_notetypes=True,
                            update_notes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            update_notetypes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            with_scheduling=False,
                            with_deck_configs=False,
                        )
                    ))
                    self.assertTrue(
                        len(result2.log.duplicate) == 1 and len(result2.log.new) == 0,
                        f'Expected second import to be a duplicate, but something else happened\nfirst log: {result1.log}\nsecond log:\n{result2.log}')
                
    def test_content_update_overwrites_previous_note(self):
        """
        Tests that bumping the modification time will update notes from previous versions.
        """
        with TemporaryDirectory() as temp_collection_dir:
            col = Collection(str(Path(temp_collection_dir) / "test.anki2"))
            with TemporaryDirectory() as first_export_dir:
                args_first = MockArgs(
                    content=CONTENT_PATH_CSV,
                    templates_dir=TEMPLATES_PATH,
                    output_dir=first_export_dir,
                    dry_run=False)
                with TemporaryDirectory() as second_export_dir:
                    args_second = MockArgs(
                        content=CONTENT_PATH_CSV_UPDATED_CONTENT,
                        templates_dir=TEMPLATES_PATH,
                        output_dir=second_export_dir,
                        dry_run=False)
                    spec_path_old = Path(f'{CONTENT_PATH_CSV}/.apkg-spec.yaml')
                    spec_path_new = Path(f'{CONTENT_PATH_CSV_UPDATED_CONTENT}/.apkg-spec.yaml')
                    first_package = export_package_from_spec(spec_path_old, args_first)
                    second_package = export_package_from_spec(spec_path_new, args_second)

                    # import the old version version
                    result1 = col.import_anki_package(ImportAnkiPackageRequest(
                        package_path=str(first_package),
                        options=ImportAnkiPackageOptions(
                            merge_notetypes=True,
                            update_notes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            update_notetypes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            with_scheduling=False,
                            with_deck_configs=False,
                        )
                    ))
                    self.assertEqual(len(result1.log.updated), 0)
                    self.assertEqual(len(result1.log.new), 1)
                    self.assertEqual(len(result1.log.duplicate), 0)

                    # now the update
                    result2 = col.import_anki_package(ImportAnkiPackageRequest(
                        package_path=str(second_package),
                        options=ImportAnkiPackageOptions(
                            merge_notetypes=True,
                            update_notes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            update_notetypes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            with_scheduling=False,
                            with_deck_configs=False,
                        )
                    ))
                    self.assertTrue(len(result2.log.updated) == 1 and len(result2.log.new) == 0 and len(result2.log.duplicate) == 0,
                                    f'Expected the note to be recognized as update.\nLog1:\n{result1.log}\nLog2:\n{result2.log}')
                    

    def test_template_update_overwrites_previous_template_csv(self):
        """
        Tests that bumping the modification time will update notes from previous versions
        of CSV content.
        """
        with TemporaryDirectory() as temp_collection_dir:
            col = Collection(str(Path(temp_collection_dir) / "test.anki2"))
            with TemporaryDirectory() as first_export_dir:
                args_first = MockArgs(
                    content=CONTENT_PATH_CSV,
                    templates_dir=TEMPLATES_PATH,
                    output_dir=first_export_dir,
                    dry_run=False)
                with TemporaryDirectory() as second_export_dir:
                    args_second = MockArgs(
                        content=CONTENT_PATH_CSV,
                        templates_dir=TEMPLATES_PATH_UPDATED,
                        output_dir=second_export_dir,
                        dry_run=False)
                    spec_path = Path(f'{CONTENT_PATH_CSV}/.apkg-spec.yaml')
                    first_package = export_package_from_spec(spec_path, args_first)
                    second_package = export_package_from_spec(spec_path, args_second)

                    # debug: uncomment to view for testing
                    #shutil.copy(first_package, './test-export-first.apkg.zip')
                    #shutil.copy(second_package, './test-export-second.apkg.zip')

                    # import the old version version
                    col.import_anki_package(ImportAnkiPackageRequest(
                        package_path=str(first_package),
                        options=ImportAnkiPackageOptions(
                            merge_notetypes=True,
                            update_notes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            update_notetypes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            with_scheduling=False,
                            with_deck_configs=False,
                        )
                    ))

                    template = find_template(col, 'Q/A Testnotetype')
                    self.assertIsNotNone(template, 'template not found in collection')
                    self.assertEqual(
                        template['qfmt'],
                        '{{Question}}')
                    self.assertEqual(
                        template['afmt'],
                        '{{Answer}}')

                    # now the update
                    result = col.import_anki_package(ImportAnkiPackageRequest(
                        package_path=str(second_package),
                        options=ImportAnkiPackageOptions(
                            merge_notetypes=True,
                            update_notes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            update_notetypes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            with_scheduling=False,
                            with_deck_configs=False,
                        )
                    ))

                    # for some reason the change is only visible if we reload the collection
                    col.close()
                    col = Collection(str(Path(temp_collection_dir) / "test.anki2"))

                    template = find_template(col, 'Q/A Testnotetype')
                    self.assertIsNotNone(template, 'template not found in collection')
                    self.assertEqual(
                        template['qfmt'],
                        'Front: {{Question}}',
                        f'Template not updated successfully, log:\n{result.log}')
                    self.assertEqual(
                        template['afmt'],
                        'Back: {{Answer}}')

    def test_template_update_overwrites_previous_template_anki(self):
        """
        Tests that bumping the modification time will update notes from previous versions
        of an imported anki package.
        """
        with TemporaryDirectory() as temp_collection_dir:
            col = Collection(str(Path(temp_collection_dir) / "test.anki2"))
            with TemporaryDirectory() as first_export_dir:
                args_first = MockArgs(
                    content=CONTENT_PATH_ANKI,
                    templates_dir=TEMPLATES_PATH,
                    output_dir=first_export_dir,
                    dry_run=False)
                with TemporaryDirectory() as second_export_dir:
                    args_second = MockArgs(
                        content=CONTENT_PATH_ANKI,
                        templates_dir=TEMPLATES_PATH_UPDATED,
                        output_dir=second_export_dir,
                        dry_run=False)
                    spec_path = Path(f'{CONTENT_PATH_ANKI}/.apkg-spec.yaml')
                    first_package = export_package_from_spec(spec_path, args_first)
                    second_package = export_package_from_spec(spec_path, args_second)

                    # debug: uncomment to view for testing
                    #shutil.copy(first_package, './test-export-first.apkg.zip')
                    #shutil.copy(second_package, './test-export-second.apkg.zip')

                    # import the old version version
                    col.import_anki_package(ImportAnkiPackageRequest(
                        package_path=str(first_package),
                        options=ImportAnkiPackageOptions(
                            merge_notetypes=True,
                            update_notes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            update_notetypes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            with_scheduling=False,
                            with_deck_configs=False,
                        )
                    ))

                    template = find_template(col, 'Q/A Testnotetype')
                    self.assertIsNotNone(template, 'template not found in collection')
                    self.assertEqual(
                        template['qfmt'],
                        '{{Question}}')
                    self.assertEqual(
                        template['afmt'],
                        '{{Answer}}')

                    # now the update
                    result = col.import_anki_package(ImportAnkiPackageRequest(
                        package_path=str(second_package),
                        options=ImportAnkiPackageOptions(
                            merge_notetypes=True,
                            update_notes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            update_notetypes=ImportAnkiPackageUpdateCondition.IMPORT_ANKI_PACKAGE_UPDATE_CONDITION_IF_NEWER,
                            with_scheduling=False,
                            with_deck_configs=False,
                        )
                    ))

                    # for some reason the change is only visible if we reload the collection
                    col.close()
                    col = Collection(str(Path(temp_collection_dir) / "test.anki2"))

                    template = find_template(col, 'Q/A Testnotetype')
                    self.assertIsNotNone(template, 'template not found in collection')
                    self.assertEqual(
                        template['qfmt'],
                        'Front: {{Question}}',
                        f'Template not updated successfully, log:\n{result.log}')
                    self.assertEqual(
                        template['afmt'],
                        'Back: {{Answer}}')

def find_template(col: Collection, name: str):
    template = None
    for name_and_id in col.models.all_names_and_ids():
        if name_and_id.name == name:
            if template is None:
                template = col.models.get(name_and_id.id)['tmpls'][0]
            else:
                raise Exception(f'Found more than one template with name {name}')
    return template