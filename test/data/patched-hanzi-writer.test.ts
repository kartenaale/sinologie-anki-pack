import {
  queryPatchedHanziWriterData
} from '../../build/gen-hanzi-data/patched-hanzi-writer'
import { cjkRadicals, kangxiRadicals } from './radicals'

describe('CJK and Kangxi radicals stroke SVG data available', () => {
  for (const radical of [...cjkRadicals, ...kangxiRadicals]) {
    test(radical, async () => {
      const { strokes } = await queryPatchedHanziWriterData(radical)
      expect(strokes.length).toBeGreaterThan(0)
    })
  }
})
