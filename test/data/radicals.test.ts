import {
  queryRadicalInfo,
  queryRadicalInline
} from '../../build/gen-hanzi-data/radical'
import { cjkRadicals, cjkRadicalsSupplement, kangxiRadicals } from './radicals'

describe(
  'CJK and Kangxi radicals have the same character set as their radical ' +
  'and a non-zero count',
  () => {
    const radicals = [
      ...cjkRadicals,
      ...kangxiRadicals
    ]
    for (const radicalIn of radicals) {
      test(radicalIn, async () => {
        const radicalOut = await queryRadicalInline(radicalIn)
        expect(radicalOut).toEqual(radicalIn)
        const { radicalInline: radicalOutFromCombined, radicalCount } =
          await queryRadicalInfo(radicalIn)
        expect(radicalOutFromCombined).toEqual(radicalIn)
        expect(radicalCount).toBeGreaterThan(0)
        expect(radicalOutFromCombined).toEqual(radicalOut)
      })
    }
  }
)

describe(
  'CJK radicals supplement characters have the same character set as their ' +
  'radical, but we accept for the moment that some do not have strokes',
  () => {
    for (const radicalIn of cjkRadicalsSupplement) {
      test(radicalIn, async () => {
        const radicalOut = await queryRadicalInline(radicalIn)
        expect(radicalOut).toEqual(radicalIn)
        const { radicalInline: radicalOutFromCombined } =
          await queryRadicalInfo(radicalIn)
        expect(radicalOutFromCombined).toEqual(radicalIn)
        expect(radicalOutFromCombined).toEqual(radicalOut)
      })
    }
  }
)
