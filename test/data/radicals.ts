import fs from 'fs'

export const kangxiRadicals: string[] = fs.readFileSync(
  'build/gen-hanzi-data/pools/simplified/kangxi-radicals.txt',
  { encoding: 'utf-8' }
).split('\n').filter(r => r !== '')
export const cjkRadicals: string[] = fs.readFileSync(
  'build/gen-hanzi-data/pools/simplified/cjk-radicals.txt',
  { encoding: 'utf-8' }
).split('\n').filter(r => r !== '')
export const cjkRadicalsSupplement: string[] = fs.readFileSync(
  'build/gen-hanzi-data/pools/simplified/cjk-radicals-supplement.txt',
  { encoding: 'utf-8' }
).split('\n').filter(r => r !== '')
