ANNOUNCEMENT_FILE := $(BUILD_PREFIX)ANNOUNCEMENT

MOSTLY_CLEAN += $(ANNOUNCEMENT_FILE)

.PHONY: announce
announce: $(ANNOUNCEMENT_FILE)

$(ANNOUNCEMENT_FILE): $(ANNOUNCE) $(PYTHON_NEEDED) build/export_apkgs.py
	$(ANNOUNCE) \
		--out $@ \
		--title '$(PACK_TITLE)' \
		--repo $(GITLAB_REPO) \
		--releaseDir $(RELEASE_DIR) \
		$(addprefix --apkg , \
			$(patsubst %,'%',$(APKGS)))
