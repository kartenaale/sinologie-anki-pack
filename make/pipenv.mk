PYTHON_LIBFAKETIME := .venv/bin/python-libfaketime
# deferred in case libfaketime is not installed yet
PYTHON = $(shell $(PYTHON_LIBFAKETIME) | sed 's/export //') PIPENV_VENV_IN_PROJECT=1 pipenv run python
PYTHON_NEEDED := .venv/.project

DEEP_CLEAN_DIRS += .venv

.PHONY: pipenv
pipenv: $(PYTHON_NEEDED)

$(PYTHON_NEEDED): Pipfile Pipfile.lock
	PIPENV_VENV_IN_PROJECT=1 pipenv install
	touch $@
