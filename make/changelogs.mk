# from newest to oldest, stop at LAST_RELEVANT_VERSION
LAST_RELEVANT_VERSION ?= 1.1.13
# get all versions from shell and drop all that are older than LAST_RELEVANT_VERSION
# this variable gets expaneded every time it is used, so fetching git tags will change it
GIT_VERSIONS = HEAD $(shell git tag --sort=-v:refname | awk '1;/$(LAST_RELEVANT_VERSION)/{exit}')
# for each version, the predecessor version followed by .. and then the version,
# for making changelogs
GIT_VERSION_RANGES = $(join \
	$(filter-out HEAD,$(GIT_VERSIONS)), \
	$(addprefix ..,$(GIT_VERSIONS)))
# files that will change if we fetch new tags => if anything is changed,
# we rebuild all changelogs (in case the history was rewritten somehow)
# deferred in case we do a fetch first
GIT_TAG_FILES = $(wildcard $(or $(GIT_DIR),.git)/refs/tags/*) $(or $(GIT_DIR),.git)/packed-refs
VER_CHANGELOGS_MARKDOWN := $(foreach VER,$(GIT_VERSIONS), \
	$(BUILD_PREFIX)CHANGELOG-$(VER).md)
VER_CHANGELOGS_HTML := $(foreach VER,$(GIT_VERSIONS), \
	$(BUILD_PREFIX)CHANGELOG-$(VER).html)
# keep CHANGELOG-HEAD.md out of combined changelog
HEAD_CHANGELOG_MARKDOWN := $(firstword $(VER_CHANGELOGS_MARKDOWN))
HEAD_CHANGELOG_HTML := $(firstword $(VER_CHANGELOGS_HTML))
VER_CHANGELOGS_HTML_IN_COMBINED = $(filter-out $(HEAD_CHANGELOG_MARKDOWN), \
	$(VER_CHANGELOGS_MARKDOWN))
COMBINED_CHANGELOG_MARKDOWN := $(BUILD_PREFIX)CHANGELOG.md
COMBINED_CHANGELOG_HTML := $(BUILD_PREFIX)CHANGELOG.html
CHANGELOGS_MARKDOWN := $(VER_CHANGELOGS_MARKDOWN) $(COMBINED_CHANGELOG_MARKDOWN)
CHANGELOGS_HTML := $(VER_CHANGELOGS_HTML) $(COMBINED_CHANGELOG_HTML)

MOSTLY_CLEAN += $(CHANGELOGS_HTML) $(CHANGELOGS_MARKDOWN)

.PHONY: changelogs
changelogs: changelogs-md changelogs-html

.PHONY: changelog-last-version
changelog-last-version: changelog-last-version-md changelog-last-version-html

.PHONY: changelog-head
changelog-head: changelog-head-md changelog-head-html

.PHONY: changelogs-md
changelogs-md: $(CHANGELOGS_MARKDOWN)

.PHONY: changelogs-html
changelogs-html: $(CHANGELOGS_HTML)

.PHONY: changelog-last-version-md
changelog-last-version-md: $(word 2,$(VER_CHANGELOGS_MARKDOWN))

.PHONY: changelog-last-version-html
changelog-last-version-html: $(word 2,$(VER_CHANGELOGS_HTML))

.PHONY: changelog-head-md
changelog-head-md: $(firstword $(VER_CHANGELOGS_MARKDOWN))

.PHONY: changelog-head-html
changelog-head-html: $(firstword $(VER_CHANGELOGS_HTML))

$(COMBINED_CHANGELOG_MARKDOWN): $(VER_CHANGELOGS_MARKDOWN)
# omit unpublished from combined changelog
	$(if $(strip $(VER_CHANGELOGS_HTML_IN_COMBINED)),cat $(VER_CHANGELOGS_HTML_IN_COMBINED),echo No versions yet) > $(COMBINED_CHANGELOG_MARKDOWN)

GIT_LOG_FORMAT := \#\# %B
CHANGELOG_VERSION = $(patsubst $(BUILD_PREFIX)CHANGELOG-%.md,%,$@)
CHANGELOG_GIT_RANGE = $(filter %..$(CHANGELOG_VERSION),$(GIT_VERSION_RANGES))
$(VER_CHANGELOGS_MARKDOWN): $(GIT_TAG_FILES)
	@$(and $(BUILD_PREFIX),mkdir -p $(BUILD_PREFIX))
	echo "# Sinologie Anki Pack $(CHANGELOG_VERSION)" > $@
	git log \
		--format=format:"$(GIT_LOG_FORMAT)" \
		--grep=^feat: --grep=^fix: ${CHANGELOG_GIT_RANGE} \
		>> $@

$(GIT_TAG_FILES):
	git fetch --tags --force

$(VER_CHANGELOGS_HTML) $(COMBINED_CHANGELOG_HTML): %.html: %.md $(YARN_INSTALLED_FILE)
	$(MARKDOWN_TO_HTML) --source $< --output $@
