REQUIRED_ON_PATH = yarn pipenv grep find sed zip cut patch git awk tr
K := $(foreach exec,$(REQUIRED_ON_PATH), \
        $(if $(shell which $(exec)),some string,$(error "No $(exec) in PATH")))

# export env BUILD_PREFIX as . or the empty empty value to build in the project
# root like in CI, otherwise all generated files are in artifacts/
# note that using . and ./ behave the same as the empty string and the filename
# targets will not start with .
BUILD_PREFIX ?= artifacts
# make empty or add trailing slash
BUILD_PREFIX := $(and $(filter-out . ./,$(BUILD_PREFIX)),$(BUILD_PREFIX)/)
# some build subprocesses need this, e.g. the JS proxy for parcel needs to
# know where to load the chardata from
export BUILD_PREFIX

# directory with apkg data and specs, can be overridden
CONTENT_DIR ?= content

# name of the repository on gitlab for download links
GITLAB_REPO ?= kartenaale/sinologie-anki-pack

PACK_TITLE ?= Sinologie Anki Pack
# part before the npm version in the release directory name
RELEASE_DIR_STEM ?= sinologie-anki-pack
VERSION ?= $(shell grep '"version":' package.json -m 1 | cut -d '"' -f 4)
