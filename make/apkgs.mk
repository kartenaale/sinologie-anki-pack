APKG_TEST_DIR := test/build
APKG_TEST_OK_FLAG := $(BUILD_PREFIX).apkg-test-ok
APKG_TEST_FILES := $(shell find $(APKG_TEST_DIR) -name '*.py' -o -name '*.yaml' -o -name '*.html' -o -name '*.csv')
CONTENT_FILES := $(shell find $(CONTENT_DIR) -name '*.yaml' -o -name '*.csv' -o -name '*.apkg' -o -name '*.png')

$(BUILD_PREFIX)apkgs.d.mk: $(filter %/.apkg-spec.yaml,$(CONTENT_FILES)) $(PYTHON_NEEDED)
	-mkdir -p $(BUILD_PREFIX)
	echo 'APKGS := \' > $@
	$(PYTHON) build/export_apkgs.py --dry-run -t no \
		-c $(CONTENT_DIR) \
		$(and $(BUILD_PREFIX),-o $(BUILD_PREFIX)) \
	| tr "\n" " " \
	>> $@
include $(BUILD_PREFIX)apkgs.d.mk

MOSTLY_CLEAN += $(APKGS) $(APKG_TEST_OK_FLAG)
CLEAN += $(BUILD_PREFIX)apkgs.d.mk

.PHONY: apkg
apkg: $(APKGS)

.PHONY: test-apkg
test-apkg: $(APKG_TEST_OK_FLAG)

$(APKG_TEST_OK_FLAG): $(PYTHON_NEEDED) $(APKG_TEST_FILES)
	$(PYTHON) -m unittest discover $(APKG_TEST_DIR)
	@$(and $(BUILD_PREFIX),mkdir -p $(BUILD_PREFIX))
	touch $(APKG_TEST_OK_FLAG)

.SECONDEXPANSION:
APKG_CONTENT_DIR = $(CONTENT_DIR)/$(subst -$(lastword $(subst -, ,$*)),,$*)
APKG_DEPS = $(filter $(APKG_CONTENT_DIR)/%,$(CONTENT_FILES))
APKG_DEPS_SORTED = \
$(filter %/.apkg-spec.yaml,$(APKG_DEPS)) \
$(sort $(filter-out %/.apkg-spec.yaml,$(APKG_DEPS)))
$(APKGS): \
$(BUILD_PREFIX)%.apkg: \
$$(APKG_DEPS_SORTED) \
build/export_apkgs.py \
$(BUILT_TEMPLATE_HTMLS) \
$(BUILT_TEMPLATE_SPECS) \
$(PYTHON_NEEDED)
	@$(and $(BUILD_PREFIX),mkdir -p $(BUILD_PREFIX))
	$(PYTHON) build/export_apkgs.py \
		-c $(dir $<) \
		-t $(BUILD_PREFIX)templates \
		$(and $(BUILD_PREFIX),-o $(BUILD_PREFIX))
