ANKI_COL_DIR := $(HOME)/.local/share/Anki2/Main
ANKI_COL_WAL := $(ANKI_COL_DIR)/collection.anki2-wal

.PHONY: install
install: $(APKGS)
	$(if $(wildcard $(ANKI_COL_WAL)), $(error Cannot install while Anki is open))
# make sure data gets overwritten by deleting old data first
	-rm $(ANKI_COL_DIR)/collection.media/_hd*.js
# not running with pipenv because we explicitly want the system anki
	build/install_apkgs.py -v \
		--collection=$(ANKI_COL_DIR)/collection.anki2 \
		$(addprefix --apkg=,$(APKGS))