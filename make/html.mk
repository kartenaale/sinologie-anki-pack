TEMPLATE_SPEC_FILENAME := .template-spec.yaml

PARCEL_DEV_ENTRY_HTMLS := src/templates/index.html
PARCEL_DEV_CONFIG := parcel-dev-config.json

TEMPLATE_FILES := $(shell find src -type f '(' -name '*.html' -o -name '*.css' -o -name '*.js' -o -name '*.ts' -o -name $(TEMPLATE_SPEC_FILENAME) ')')
SRC_TEMPLATE_TS := $(filter %.ts,$(TEMPLATE_FILES))
SRC_TEMPLATE_SPECS := $(filter %$(TEMPLATE_SPEC_FILENAME),$(TEMPLATE_FILES))
SRC_TEMPLATE_HTMLS := \
$(filter %/front.html,$(TEMPLATE_FILES)) \
$(filter %/back.html,$(TEMPLATE_FILES))
SRC_TEMPLATE_TESTS := $(shell find test/templates -type f -name '*.test.ts')
HTML_TEST_OK_FLAG := $(BUILD_PREFIX).html-test-ok
BUILT_TEMPLATE_SPECS := $(patsubst src/%,$(BUILD_PREFIX)%,$(SRC_TEMPLATE_SPECS))
BUILT_TEMPLATE_HTMLS := $(patsubst src/%,$(BUILD_PREFIX)%,$(SRC_TEMPLATE_HTMLS))

TS_SRC += $(SRC_TEMPLATE_TS) $(SRC_TEMPLATE_TESTS)
MOSTLY_CLEAN += $(BUILT_TEMPLATE_SPECS) $(BUILT_TEMPLATE_HTMLS) $(HTML_TEST_OK_FLAG)
DEEP_CLEAN_DIRS += .parcel-cache dist

.PHONY: html
html: $(BUILT_TEMPLATE_HTMLS) $(BUILT_TEMPLATE_SPECS)

.PHONY: dev
dev: $(PARCEL) $(HANZI_DATA)
	$(PARCEL) serve $(PARCEL_DEV_ENTRY_HTMLS) --config=./$(PARCEL_DEV_CONFIG)

.PHONY: test-html
test-html: $(HTML_TEST_OK_FLAG)

$(HTML_TEST_OK_FLAG): $(JEST) $(HANZI_DATA) $(SRC_TEMPLATE_TESTS) $(SRC_TEMPLATE_TS) $(SRC_COMMON_TS)
	$(JEST) $(SRC_TEMPLATE_TESTS)
	@$(and $(BUILD_PREFIX),mkdir -p $(BUILD_PREFIX))
	touch $(HTML_TEST_OK_FLAG)

# no parcel plugin or config required for production builds, Anki handles the
# tags
$(BUILT_TEMPLATE_HTMLS) &: $(HANZI_DATA) $(PARCEL) $(TEMPLATE_FILES) $(SRC_COMMON_TS)
	@$(and $(BUILD_PREFIX),mkdir -p $(BUILD_PREFIX))
	$(PARCEL) build --no-source-maps --dist-dir=$(BUILD_PREFIX)templates $(SRC_TEMPLATE_HTMLS)
# set moddat even for files that did not need to be updated (to avoid redundant rebuilds)
	touch $(BUILT_TEMPLATE_HTMLS)

$(BUILT_TEMPLATE_SPECS): $(BUILD_PREFIX)%: src/%
	mkdir -p $(dir $@)
	cp $< $@
