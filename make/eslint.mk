LINT_OK_FLAG := $(BUILD_PREFIX).lint-ok

.PHONY: lint
lint: $(LINT_OK_FLAG)

.PHONY: fix
fix: $(ESLINT)
	$(ESLINT) --fix $(TS_SRC)

$(LINT_OK_FLAG): $(ESLINT) $(TS_SRC)
	$(ESLINT) $(TS_SRC)
	@$(and $(BUILD_PREFIX),mkdir -p $(BUILD_PREFIX))
	touch $@