DOCS_MARKDOWN := $(filter-out CHANGELOG%,$(filter-out README.md,$(wildcard *.md)))
DOCS_HTML := $(patsubst %.md,$(BUILD_PREFIX)%.html,$(DOCS_MARKDOWN))

MOSTLY_CLEAN += $(DOCS_HTML)

.PHONY: docs
docs: $(DOCS_HTML)

$(DOCS_HTML): $(BUILD_PREFIX)%.html: %.md $(MARKDOWN_TO_HTML)
	$(MARKDOWN_TO_HTML) --source $< --output $@
