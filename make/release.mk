RELEASE_DIR := $(BUILD_PREFIX)$(RELEASE_DIR_STEM)-$(VERSION)
RELEASE_ZIP := $(RELEASE_DIR).zip

MOSTLY_CLEAN += $(RELEASE_ZIP)
MOSTLY_CLEAN_DIRS += $(RELEASE_DIR)

.PHONY: release
release: $(RELEASE_DIR)

.PHONY: release-zip
release-zip: $(RELEASE_ZIP)

$(RELEASE_DIR): \
$(DOCS_HTML) \
$(COMBINED_CHANGELOG_HTML) \
$(APKGS) \
NOTICE \
$(wildcard screenshots/*.jpg) $(wildcard screenshots/*.png)
# always start from a clean release dir, also ensures a fresh timestamp on dir
	rm -rf $(RELEASE_DIR)
	mkdir -p $(RELEASE_DIR)/screenshots
	cp -t $@ $(filter-out screenshots/%,$^)
	cp -t $@/screenshots $(filter screenshots/%,$^)

$(RELEASE_ZIP): $(RELEASE_DIR)
	zip -r $(RELEASE_ZIP) $(RELEASE_DIR)
