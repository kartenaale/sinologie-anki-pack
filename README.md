# ⚠️ Archived!
This pack is of an old version that is no longer maintained. Please check out
the new Anki packs instead: https://gitlab.phaidra.org/kartenaale/packs

For example, if you are new to studying sinology, you probably want the pack
[Sinologie STEOP](https://gitlab.phaidra.org/kartenaale/packs/sinology-1).

# Sinologie Anki Pack (archived/discontinued)

Hi there! If you just want to learn, see the user documentation in [English](GUIDE.md)
or [German](ANLEITUNG.md). If you like coding and are interested in how it
works or want to contribute code, please read on.

This is a template for flashcard decks for the popular _Anki_ app, mostly to
learn facts and the Chinese languages. You can use the bundled vocabulary or
the HTML this template generates for other purposes too.

The template and content is targeted at native speakers of _German_ and includes cards to practice:
* listening to and understanding Chinese speech,
* reading Hànzì and Pīnyīn,
* writing Hànzì,
* translating German words and expressions to Chinese,
* identify the radical of a character.

You can use this deck as-is or customize it to your needs. For example, you
can add new words within Anki, or you can change the translations of existing
words to some other language if you don't speak German. You don't need any
coding skills to do any of this.

For example, radical practice looks like this when the answer is revealed, but
only the chinese character needs to be specified in your note, the rest is done
by the template:

![Screenshot: radical practice card, back side](screenshots/screenshot-radical-practice.png)

If you like coding, you can also check out this repository and customize the
template for your own needs. See below for details.

## Building
Read this section if you want to make a customized version of the template,
e.g. to change the name of the fields to match your pre-existing notes, or if
you want to add entirely new features.

### Overview
What this whole thing does is generate a bunch of HTML files and then bake
them into anki packages (APKG) files with bundled vocabulary using python.
The build process is controlled with _make_.

### Prerequesites
Make sure that you have _node.js_, _yarn_, _pipenv_, _zip_, and _make_
installed.

Then, check out the repo:

    git clone git@gitlab.phaidra.org:kartenaale/sinologie-anki-pack.git
    cd sinologie-anki-pack

### Applying Changes
Run a dev server at https://localhost:1234/:
    
    make dev

If you are seeing a collection of links to the various templates at
https://localhost:1234/, it worked.

You can modify the templates by playing with the HTML, CSS and JS files in the
`src` directory. Any changes should be visible without refreshing your tab
during development.

### Building HTML and apkg
When you are happy with your changes, run:

    make release

If everything worked out, you should have a directory like
`artifacts/sinologie-anki-pack-1.2.3` containing freshly baked APKGs, with your
changes to `src/` and the vocabulary and other content in `content/`.0

Please note that the build is severely under-tested. If you are experiencing
issues with the build on your system, feel free to file a bug. If the build
complains about a missing tool on your `PATH`, check that you can run all of
the following commands without error anywhere on your machine:

    yarn; pipenv; grep; find; sed; zip; cut; patch; git; awk; tr

### Customizing vocabulary in release
The vocabulary or other content is stored in `content/`. The version of the
vocabulary as well as some other stuff are configured in the `.apkg-spec.yaml`
files in the content subfolders.

If you want to add more packs or extend them, you will have to do without
documentation for the moment, but try to do something similar as in the
other packs or raise an issue on Gitlab and ask for help.

## Contributing
Feel free to raise an issue if you are experiencing issues or if you have any
suggestions or even pull requests to make this template better.

If you want to contribute new words but are not that technical, open an issue
and post the new words there as an Anki collection or some other format of your
choice.
