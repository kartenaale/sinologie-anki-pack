import type { StrokeType } from './stroke-encodings'

export interface HanziData {
  /**
   * Radical of the character.
   *
   * In traditional mode, this is the full character instead of the form used
   * in the char.
   */
  radical: string
  /** e.g. 1+0 for yi1 */
  count: string
  /**
   * For each stroke, gets a symbol identifying the type of stroke, e.g. pie is
   * StrokeType.PIE.
   */
  strokeTypes: StrokeType[]
  /** Stroke SVG paths for hanzi-writer */
  strokes: string[]
  /** Also for hanzi-writer */
  medians: number[][][]
  /** Indices of radical strokes. */
  radStrokes?: number[]
  /**
   * If values differ for traditional, only the changed props are stored here.
   * For example, this will typically show the full character form of the
   * radical.
   */
  trad?: TradOverrides
}

export type TradOverrides = Partial<Omit<HanziData, 'trad'>>
