const nightMode = document.getElementsByClassName('nightMode').length > 0

export const colorInactive = nightMode ? '#909090' : '#999999'
export const colorFg = nightMode ? '#FFFFFF' : '#222222'
export const colorHighlight = '#339966'

/** Side length of character SVGs. */
export const hanziSize = 85
export const hanziSizeLarge = 140
