import type { HanziData } from '../../../src-common/hanzi-data'
import { lookup } from './lut'
import { readFile } from 'fs/promises'

export interface GetHanziDataOpts {
  char: string
  kind?: GetHanziDataKind
}

export const enum GetHanziDataKind {
  ANY = 'any',
  DEFAULT = 'default',
  TRADITIONAL = 'traditional'
}

export async function getHanziProp (
  { prop, ...opts }: GetHanziDataOpts & { prop: string }
): Promise<string | string[] | undefined> {
  const data = await getHanziData(opts)
  if (data === undefined) {
    return undefined
  }
  return data[prop] ?? lookup(data, prop)
}

const dataFilePrefix = '_hd'
const jsonpCallbackFnName = 'hd'
/** chars against functions to call on resolve. */
const jsonpWaiters = new Map<string, Array<(HanziData) => void>>()
const cached: Record<string, Readonly<HanziData>> = {}
const canAccessFilesystem = typeof document === 'undefined'

export async function getHanziData (
  { char, kind }: GetHanziDataOpts
): Promise<Readonly<HanziData>> {
  if (char.length !== 1) {
    throw new Error(`Can only get data for single chars, got: ${char}`)
  }

  let data: Readonly<HanziData>
  if (char in cached) {
    data = cached[char]
  } else if (canAccessFilesystem) {
    // read directly from disk when running in test and skip the cache
    data = await fetchNonBrowser(char)
  } else {
    // in production and parcel use a JSONP-like API because everything else
    // failed in AnkiDroid
    data = await fetchJsonp(char)
    // caching helps a lot with load speed since the same character often
    // occurs multiple times on the same card
    cached[char] = data
  }
  if (kind === GetHanziDataKind.TRADITIONAL && data.trad !== undefined) {
    // data in the cache is read-only => make a fresh copy with trad overrides
    return {
      ...data,
      ...data.trad
    }
  }
  return data
}

async function fetchNonBrowser (char: string): Promise<HanziData> {
  if (!('BUILD_PREFIX' in process.env)) {
    throw new Error('Dunno where the chardata at, what BUILD_PREFIX?')
  }
  const dir = `${process.env.BUILD_PREFIX}hanzi-data`
  const path = `${dir}/${dataFilePrefix}${char}.js`
  const jsonp = await readFile(path, { encoding: 'utf-8' })
  const data = JSON.parse(jsonp.substring(
    'hd("A",'.length,
    jsonp.length - ')'.length
  ))
  // just for testing: we want to catch accidental modifications of data that
  //                   would be shared if this were production
  Object.freeze(data)
  return data
}

/**
 * Callback for the JSONP scripts with the data.
 *
 * Raw JSON would not work with AnkiDroid, which does not support XHR/fetch,
 * but allows scripts.
 *
 * In AnkiDroid, every card gets a fresh window, but desktop and Anki Web
 * preserved the callback from the last call. If window already contains a
 * callback, it works to just overwrite the old callback here that is no longer
 * needed. See #298 for details.
 */
if (!canAccessFilesystem) {
  window[jsonpCallbackFnName] = (char: string, data: HanziData) => {
    (jsonpWaiters.get(char) ?? []).forEach(w => {
      // careful: every consumer gets an identical object, don't modify
      w(data)
    })
    jsonpWaiters.delete(char)
  }
}

/**
 * Registers a waiter for the specified char.
 *
 * It will be removed automatically after the data has been received.
 */
function registerJsonpWaiter (
  char: string,
  callback: (data: HanziData) => void
): void {
  jsonpWaiters.set(
    char,
    [...(jsonpWaiters.get(char) ?? []), callback]
  )
}

/**
 * Fetches the character data (strokes, SVG, etc.) for the specified character.
 */
async function fetchJsonp (char: string): Promise<Readonly<HanziData>> {
  return await new Promise((resolve, reject) => {
    const prefix = process.env.NODE_ENV === 'production'
      // No directories for Anki media possible, for prod use just filename
      ? ''
      // For testing in parcel it works better to have a subpath that we can
      // exclude from parcel.
      : '/hanzi-data/'
    const url = `${prefix}${dataFilePrefix}${char}.js`
    if (document.querySelector(`script[src="${url}"]`) !== null) {
      // already being fetched, append to the list of existing waiters
      registerJsonpWaiter(char, resolve)
    } else {
      // no fetch in flight, inject a new script tag
      const script = document.createElement('script')
      registerJsonpWaiter(
        char,
        data => {
          script.remove()
          resolve(data)
        }
      )
      script.async = true
      script.src = url
      script.onerror = () => {
        script.remove()
        reject(new Error(`No character data available for ${char}`))
      }
      document.body.appendChild(script)
    }
  })
}
