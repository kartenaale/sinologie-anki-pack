import { GetHanziDataKind, getHanziProp } from '../hanzi-data'
import { isMaybeHanzi } from '../is-hanzi'

export async function init (within: HTMLElement): Promise<void> {
  await Promise.all(
    Array.prototype.map.call(
      within.querySelectorAll('[hanzi-data]'),
      async (element: HTMLElement) => {
        if (element.classList.contains('is-initialized') ||
            element.classList.contains('is-loading')) {
          return
        }
        element.classList.add('is-loading')

        const chars = element.getAttribute('hanzi-data')
        if (chars === null) {
          throw new Error('unreachable, hanzi-data was queried')
        }

        const delimiter = element.getAttribute('hanzi-delimiter') ?? ';'

        const kindAttr = element.getAttribute('hanzi-kind')
        let kind = GetHanziDataKind.DEFAULT
        if (kindAttr === 'traditional') {
          kind = GetHanziDataKind.TRADITIONAL
        }

        const prop = element.getAttribute('hanzi-prop')
        if (prop === null) {
          throw new Error('hanzi-prop missing')
        }

        let dataAsText = ''
        for (const char of chars) {
          // ignore non-hanzi like ellipsis characters and spaces
          if (isMaybeHanzi(char)) {
            const value = await getHanziProp({ char, kind, prop })
            if (value !== undefined) {
              if (dataAsText !== '') {
                dataAsText += `${delimiter} `
              }
              if (typeof value === 'string') {
                dataAsText += value
              } else {
                dataAsText += JSON.stringify(value)
              }
            }
          }
          element.textContent = dataAsText
          element.classList.add('is-initialized')
          element.classList.remove('is-loading')
        }
      }
    )
  )
}
