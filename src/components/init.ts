import { init as initHanziData } from './hanzi-data/init'
import { init as initTts } from './tts/init'
import { init as initWrite } from './write/init'

export async function init (within: HTMLElement | null): Promise<void> {
  if (within === null) {
    throw new Error('container not found')
  }
  await initHanziData(within)
  initTts(within)
  await initWrite(within)
}
