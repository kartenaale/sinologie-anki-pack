## dockerfile used for the gitlab build runner

# we are using debian because alpine cannot install anki via pipenv for some reason
FROM debian:12.4

RUN apt-get update && apt-get install -y \
faketime \
git \
make \
nodejs \
npm \
pipenv \
python3 \
python3-pip \
zip

RUN npm install --global \
yarn \
mudslide
