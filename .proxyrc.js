const process = require('process')
const serveStatic = require("serve-static")
module.exports = function (app) {
  console.error(('proxy config'))
  if (!('BUILD_PREFIX' in process.env)) {
    throw new Error(`Dunno where the chardata at, what BUILD_PREFIX?`)
  }
  const dir = `${process.env.BUILD_PREFIX}hanzi-data`
  app.use("/hanzi-data", serveStatic(dir))
  console.error(('ok configured at '), dir)
}
